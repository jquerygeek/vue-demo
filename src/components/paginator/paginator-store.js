export const paginationModule = {
    //namespaced: true,
    state: { 
        currentPage: 1
    },
    mutations: {
        increase (state) {
            state.currentPage++
        },

        decrease (state) {
            state.currentPage--
        },

        changePage (state, page) {
            state.currentPage = page;
        }
    },

    getters: {
        currentPage (state) {
            return state.currentPage;
        }
    }
}