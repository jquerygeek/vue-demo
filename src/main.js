
import Vue from 'vue'

import store from './store/app-store'
import VueApp from './vueApp'
//import router from './router'
//import vueResource from 'vue-resource'
import vueCustomElement from 'vue-custom-element';

import UserList from './components/UserList'
import MessageForm from './components/MessageForm'
import MessageList from './components/MessageList'
import Badges from './components/Badges'
import TodoList from './components/TodoList'
import TodoCompleted from './components/TodoCompleted'


//Vue.use(vueResource)
Vue.use(vueCustomElement)

Vue.customElement('user-list', UserList, {
  shadow: true,
  shadowCss: `
  li {
    margin: 2px 10px;
    padding: 5px;
    background-color: #eeefff;
    border: 1px solid #666;
  }`
});

Vue.customElement('message-form', MessageForm, {
  shadow: false
});

Vue.customElement('message-list', MessageList, {
  shadow: true,
  shadowCss: `
  .readable {
     text-decoration: line-through;
  }`
});

Vue.customElement('badges-box', Badges, {
  shadow: false
});

Vue.customElement('todo-list', TodoList, {
  shadow: false
});

Vue.customElement('todo-completed', TodoCompleted, {
  shadow: false
});

new Vue({
  el: '#vueApp',
  template: '<vue-app></vue-app>',
  store,
  components: { VueApp }
})
