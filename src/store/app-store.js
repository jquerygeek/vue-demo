import Vue from 'vue';
import Vuex from 'vuex';
import { paginationModule } from '../components/paginator/paginator-store'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        completed: [],
        todos: []
    },
    modules: {
        paginationModule
    },
    getters: {
        todos(state) {
            return state.todos.filter( todo => {
                return !todo.completed;
            });
        },

        todosNumber(state) {
            return state.todos.filter( todo => {
                return !todo.completed;
            }).length; 
        },

        limitedTodos(state) {
            var uncompletedTodos = state.todos.filter( todo => {
                return !todo.completed;
            });;
            return (start, total) => {
                let end = parseInt(start + total);
                return uncompletedTodos.slice(start, end)
            };
        },

        completedTodos(state) {
            return state.completed;
        }
    },
    mutations: {
        doneTask(state, todoId) {
            const todo = state.todos.find( todo => {
                return todo.id == todoId;
            })
            todo.completed = true;
            const completedTodo = {
                id: todo.id,
                title: todo.title,
                completed: true
            }
            state.completed.push(completedTodo);
            console.log(state.completed)
        },

        undoCompleted(state, todoId) {
            const todo = state.todos.find( todo => {
                return todo.id == todoId;
            })
            todo.completed = false;
            const completed = state.completed.find( todo => {
                return todo.id == todoId;
            })
            state.completed.splice(state.completed.indexOf(completed), 1);
        }
    }
});