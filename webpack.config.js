"use strict";

let path = require('path');
let webpack = require('webpack');

let entryPoint = path.resolve(__dirname + '/src/main.js');
let outputPath = path.resolve(__dirname, '/build');
let fileName = 'app.js';

let plugins = [];
let env = process.env.WEBPACK_ENV;

// Main webpack config
module.exports = {
    context: __dirname,
	entry: {
		app: [ entryPoint ]
	},
	output: {
		path: outputPath,
		filename: fileName,
        publicPath: 'build'
	},
	module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ]
    },
    resolve: {
        extensions: ['*', '.js', '.vue', '.json'],
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        }
    }
};